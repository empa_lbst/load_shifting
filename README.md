Profile shifting optimization model

The model gives optimally shifted profiles so that the total peak of all profiles for each timestep does not exceed sum of individual peaks multiplied by a given simultaneity factor. The shifting is based on exchanging two or more data points (loads) in time. The model builds upon “bin packing problem” and it is a combinatorial MILP model. 

The main parameters/values/constraints/procedures are the following: 

* **Timestep** : number of timesteps that the profiles have
* **NumberOfProfiles**: Number of individual profiles
* **Profiles(p,i)**: parameter containing all profiles
*	**PenaltyCost(i,j)**: parameter that gives the cost of shifting two data  points (loads) in time. The further they are, the more ‘expensive’ it is. The idea is that the shifting is done in such a way that the new shifted profiles look as similar as possible to the original ones. 
*	**NewProfileALL(p,i)**: parameter that contains new shifted profiles once the optimization has finished.
*	**NewProfileConst(sub_i)**: The number value has to be changed to the needed new total maximum peak.
*	**Constr2(p)**: It can be added to limit the maximum number shifts per profile is defined.
*	**Days**: Change ‘to’ to a number depending on within how many days profiles should/can be shifted. E.g. from:1, to: 365 means that only within a day (24 hours) profiles can be shifted (a year has 365 days).
*	**HourWeek**: It containts a procedure for rolling the period within which shifting occurs. Change the number in ‘Subset_Time’ and ‘NewProfileAll’ (currently 24) to a number of hours that a period in Days has hours. E.g. if every 5 days should be considered, the number in ‘Subset_Time’ and ‘NewProfileAll’ has to be changed to  24*5. Also, ‘Days’ has to be changed to ‘from:1, to: 73’.

In order to run the model, modify the above mentioned parameters/values/constraints/procedures and run ‘HourWeek’ procedure. The new shifted profiles will be saved in ‘NewProfileALL(p,i)’.